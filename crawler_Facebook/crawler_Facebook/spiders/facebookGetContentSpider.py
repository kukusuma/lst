#!/usr/bin/python
# -*- coding: UTF-8 -*-
import json
import re
import os
import time
import string
import datetime
import argparse
from scrapy.http import FormRequest
from scrapy.spider import Spider
from scrapy.http import Request
    
class facebookContentSpider(Spider):
	#----------------------------------------------------------------------
	# Initial	
	def __init__(self,**kw):
		# get email, password, pagename
		#self.email = str(kw.get('email'))
		#self.password = str(kw.get('password'))  
		self.access_token = kw.get('access_token')
		self.page_id = kw.get('pageName') 
		self.post = kw.get('post')
		self.output_path = kw.get('output_path')
		self.index = 1 
		self.data = dict()
		self.first_timestamp = " "
		# set start url, domain, declare global variable
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com']
		DOWNLOAD_DELAY = 1.0
		CONCURRENT_REQUESTS = 10
		
	#----------------------------------------------------------------------
	# Function sent email and password log in 
	def parse(self, response):
		# Form Reequest url
		return Request(url='https://graph.facebook.com/v2.4/'+self.page_id+'/feed?fields=message,link,created_time,actions,to,full_picture\
		&date_format=U&access_token='+self.access_token, callback=self.get_content)	
		
	#----------------------------------------------------------------------
	#get data and write files
	def get_content(self,response):
		first_post = 0
		page_data = json.loads(response.body)
		chars=  re.escape(string.punctuation)
		chars+="\n,\r"
		chars+=u'\uff01,\uff1f,\u3002,\u2665'
		#get id_page,post,message,time,link_post to jsonfile
		if page_data['paging']['next'] != None:
			for index in page_data['data']:
				if index.has_key('message'):
					if self.post == "admin":
						if 'to' in index.keys():
							continue
					if first_post == 0:
						self.data = {'page_id' : self.page_id}
						content = {'content':[]}
						self.data.update(content)
						first_post = first_post+1
						#chang time to unix timestamp
						self.first_timestamp =  str(index['created_time'])
						
					#chang time to unix timestamp
					timestamp = index['created_time']
					post_id = index['id'].split('_')
					msg = index['message']
					
					#escape special characters
					msg = re.sub(r'['+chars+']', ' ',msg)
					if 'full_picture' in index.keys():
						picture = index['full_picture']
						content = {'message':msg,'code':post_id[1],'link':index['actions'][0]['link'],'created_time':timestamp,'picture':picture}
					else:
						content = {'message':msg,'code':post_id[1],'link':index['actions'][0]['link'],'created_time':timestamp}
					self.data['content'].append(content)
					
			#write file
			path = self.output_path+str(self.page_id)
			if not os.path.exists(path): 
				os.makedirs(path)	
			filename = os.path.join(path,str(self.page_id)+'_'+self.first_timestamp+'_'+str(self.index)+'.json')
			write_data = open(filename,'a')	
			json_string = json.dumps(self.data,ensure_ascii = False,sort_keys=True,indent=2).encode('utf8')
			json_string.decode('utf8')
			write_data.write(json_string)
			
		#change page
		try :
			nextpage = page_data['paging']['next']
			self.index = self.index+1
			yield Request(url=nextpage, callback=self.get_content)
		except :
			nextpage = "last page"
