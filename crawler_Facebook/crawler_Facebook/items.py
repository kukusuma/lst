# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerFacebookItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    id_f = scrapy.Field()
    name = scrapy.Field()
    message = scrapy.Field()
    title = scrapy.Field()
    pass
