# -*- coding: utf-8 -*-

# Scrapy settings for crawler_Facebook project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'crawler_Facebook'

SPIDER_MODULES = ['crawler_Facebook.spiders']
NEWSPIDER_MODULE = 'crawler_Facebook.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'crawler_Facebook (+http://www.yourdomain.com)'
