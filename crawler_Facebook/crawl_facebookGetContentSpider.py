import sys
import argparse
from twisted.internet import reactor, task
from scrapy.crawler import Crawler 
from scrapy import log, signals
from crawler_Facebook.spiders.facebookGetContentSpider import facebookContentSpider
from scrapy.utils.project import get_project_settings

#----------------------------------------------------------------------
	#Argument Parser
parser = argparse.ArgumentParser(description='facebook Crawler')
parser.add_argument('token',type = str,action = 'store',help = 'Access Token')
#parser.add_argument('e',type = str,action = 'store',help = 'email')
#parser.add_argument('p',type = str,action = 'store',help = 'pass')
parser.add_argument('id',type = str,action = 'store',help = 'Page name(id)')		
parser.add_argument('-p','--post',dest = 'post',choices=['all', 'admin'],action = 'store',help = 'Choose post by whom\
(default=admin)' ,default = 'admin')		
parser.add_argument('-o','--output-path',dest = 'output_path',type = str,action = 'store',help = 'Directory for write file\
(default=./crawler_output/)',default = './crawler_output/' )
def crawl_content():
	args = parser.parse_args()
	#spider = facebookContentSpider(email=args.e,passw=args.p,pageName=args.id,post = args.post,output_path = args.output_path) #set spider 
	spider = facebookContentSpider(access_token=args.token,pageName=args.id,post = args.post,output_path = args.output_path) #set spider 
	settings = get_project_settings()
	crawler = Crawler(settings)
	crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
	crawler.configure()
	crawler.crawl(spider) #crawl spider
	crawler.start()
	log.start()
	reactor.run() 
def main():
	crawl_content()
	
if __name__ == "__main__":
	main()
