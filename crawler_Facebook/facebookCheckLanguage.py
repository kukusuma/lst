# -*- coding: UTF-8 -*-
import re	
import sys
import os
import json 
import io
import argparse

sentence_container = dict()

#----------------------------------------------------------------------
	#Argument Parser
parser = argparse.ArgumentParser(description='Group Language and write file')
parser.add_argument('path_input',type = str,action = 'store',help = 'Directory for read crawler output file (.json)')
parser.add_argument('-o','--output-path',dest = 'output_path',type = str,action = 'store',help = 'Directory for write file \
(default =./checklanguage_output/)',default = './checklanguage_output/' )		
parser.add_argument('-t','--output-type',dest = 'output_type',choices=['text', 'json','both'],action = 'store',help = 'Choose file type\
(default = .json)' ,default = 'json')
		
#---------------------------------------------------------------------------	
#read all file in folder
def directory_walk(dir_name):
	outputList = []
	exclude = set(['url_topic','Topic'])
	for root, dirs, files in os.walk(dir_name):
		dirs[:] = [d for d in dirs if d not in exclude]
		for f1 in files:
			outputList.append('/'.join([root, f1]))
	return outputList
	
#---------------------------------------------------------------------------		
#check and group language ตรวจสอบภาษาและจัดกลุ่ม
def languageCheck(sentence,sentence_dictionary):
	result_list = []
	regx_thai = re.compile(u'[\u0E00-\u0E7F]+', re.UNICODE)
	regx_chineseB = re.compile(u'[\u20000-\u2A6DF]+', re.UNICODE)
	regx_chineseC = re.compile(u'[\u2A700-\u2B73F]+', re.UNICODE)
	regx_chineseD = re.compile(u'[\u2B740-\u2B81F]+', re.UNICODE)
	regx_myanmar = re.compile(u'[\u1000-\u109F]+', re.UNICODE)
	regx_eng = re.compile(u'[\u0041-\u024F]+',re.UNICODE)
	regx_lao = re.compile(u'[\u0E80-\u0EFF]+', re.UNICODE)
	regx_cambodia  = re.compile(u'[\u1780-\u17FF]+', re.UNICODE)
	regx_number = re.compile(u'[\u0030-\u0039]+',re.UNICODE)
	if regx_number.match(sentence):
		return
	if regx_thai.match(sentence):
		if sentence_dictionary.has_key('TH'):
			sentence_dictionary['TH'].append(sentence)
		else:
			TH = {'TH':[]}
			sentence_dictionary.update(TH)
			sentence_dictionary['TH'].append(sentence)
	elif regx_eng.match(sentence):
		if sentence_dictionary.has_key('EN'):
			sentence_dictionary['EN'].append(sentence)
		else:
			EN = {'EN':[]}
			sentence_dictionary.update(EN)
			sentence_dictionary['EN'].append(sentence)		
	elif regx_myanmar.match(sentence):
		if sentence_dictionary.has_key('MM'):
			sentence_dictionary['MM'].append(sentence)
		else:
			MM = {'MM':[]}
			sentence_dictionary.update(MM)
			sentence_dictionary['MM'].append(sentence)				
	elif regx_lao.match(sentence):
		if sentence_dictionary.has_key('LA'):
			sentence_dictionary['LA'].append(sentence)
		else:
			LA = {'LA':[]}
			sentence_dictionary.update(LA)
			sentence_dictionary['LA'].append(sentence)				
	elif regx_cambodia.match(sentence):
		if sentence_dictionary.has_key('KH'):
			sentence_dictionary['KH'].append(sentence)
		else:
			KH = {'KH':[]}
			sentence_dictionary.update(KH)
			sentence_dictionary['KH'].append(sentence)				
	elif regx_chineseB.match(sentence.encode('utf8')):
		if sentence_dictionary.has_key('CN'):
			sentence_dictionary['CN'].append(sentence)
		else:
			chi = {'CN':[]}
			sentence_dictionary.update(chi)
			sentence_dictionary['CN'].append(sentence)
			
#---------------------------------------------------------------------------					
#detector sentence กำจัดข้อความที่ไม่เกี่ยวข้อง	
def detector(sentence_dictionary):
	if len(sentence_dictionary.keys())>=2:
		return True
	else:
		return False
		
#---------------------------------------------------------------------------	
#getandstore data อ่านไฟล์และเก็บข้อมูลลงdict 
def getContent(path):
	language = {'language' : []}
	sentence_container.update(language)
	files = directory_walk(path)
	#read file
	for file in files:
		lines = io.open(file, mode='rt', encoding='utf-8', errors='replace')
		json_f = json.load(lines)
		for post in json_f['content']:
			msg =  post['message']
			if 'www' in msg:
				continue
			msg = msg.split()
			sentence_dictionary = dict()
			for sentence in msg:
				languageCheck(sentence,sentence_dictionary)
			result = detector(sentence_dictionary)
			if result == True:
				sentence_container['language'].append(sentence_dictionary)	
		lines.close()
		
#---------------------------------------------------------------------------			
#write json file
def write_Json(path,page_id):
	if not os.path.exists(path): 
		os.makedirs(path)	
	filename = os.path.join(path,page_id+'.json')
	write_data = open(filename,'a')	
	json_string = json.dumps(sentence_container,ensure_ascii = False,sort_keys=True,indent=2).encode('utf8')
	json_string.decode('utf8')
	write_data.write(json_string)
	write_data.close()
	
#---------------------------------------------------------------------------	
#write text file	
def write_text(path,page_id):
	if not os.path.exists(path): 
		os.makedirs(path)
	filename = os.path.join(path,page_id+'.txt')	
	write_data = open(filename,'a')	
	length_key = 0
	key = " "
	#check key in dict
	for sentence in sentence_container['language']:
		if len(sentence.keys())>length_key:
			key = sentence.keys()
			length_key = len(sentence.keys())
		if sentence.keys() not in key:
			length = len(sentence.keys())
			tence =  sentence.keys()[length-1]
			if tence not in key:
				key.insert(length_key,tence)
				length = length-1
	key_text = ','.join(key)
	key_text+='\n'
	write_data.write(key_text)
	#write data to text
	for sentence in sentence_container['language']:	
		length = length_key
		sentence_list = []
		for index in range(0,length):
			if length >=0:
				if sentence.has_key(key[length_key-length]):
					msg = '|'.join(sentence[key[length_key-length]])
					sentence_list.insert(length_key-length,msg)
				length= length-1
		sentence_text  = ','.join(sentence_list)
		sentence_text+='\n'
		write_data.write(sentence_text.encode('utf8'))	
	write_data.close()
	
#---------------------------------------------------------------------------		
#main
def main():
	args = parser.parse_args()
	getContent(args.path_input)
	page_id = args.path_input.split('/')
	page_id = page_id[len(page_id)-1]
	
	#choose type to write
	if args.output_type == 'text':
		write_text(args.output_path,page_id)
	elif args.output_type == 'json':
		write_Json(args.output_path,page_id)
	else:
		write_Json(args.output_path,page_id)
		write_text(args.output_path,page_id)
	
if __name__ == "__main__":
	main()
	
