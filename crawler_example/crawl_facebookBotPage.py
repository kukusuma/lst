from twisted.internet import reactor, task
from scrapy.crawler import Crawler 
from scrapy import log, signals
from wordlist.spiders.facebookBot_spider import facebookBotSpider
from scrapy.utils.project import get_project_settings

def crawl_postid(u,p,ac,page):
	spider = facebookBotSpider(email=u,password=p,access_token=ac,page_id=page) #set spider 
	settings = get_project_settings()
	crawler = Crawler(settings)
	crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
	crawler.configure()
	crawler.crawl(spider) #crawl spider
	crawler.start()
	#log.start()
	reactor.run() 

input_data   = raw_input()
data         = input_data.split(' ')
email        = data[0]
password     = data[1]
access_token = data[2]
page         = data[3]
crawl_postid(email,password,access_token,page)

