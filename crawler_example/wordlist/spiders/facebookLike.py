import json
import re
import os
import time
from scrapy.http import FormRequest
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
from scrapy import log
    
class facebookLikeSpider(Spider):
	
	#----------------------------------------------------------------------
	# Initial	
	def __init__(self,**kw):
		
		# get email, password, pagename
		self.email = str(kw.get('email'))
		self.password = str(kw.get('password'))  
		self.page_id = str(kw.get('pageId'))
		self.access_token= str(kw.get('access_token'))

		# set start url, domain, declare global variable
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com/login.php']
		DOWNLOAD_DELAY = 1.0
		CONCURRENT_REQUESTS = 10
		self.save_likes = dict() 
		self.date = time.strftime("%d-%m-%y") 
		self.path = '../InfoMine/Corpus/facebook/'+self.page_id+'/'+self.date
		try :	
			self.file_postid  = open(self.path+'/PostId'+self.date+'.txt')
			self.post_id      = self.file_postid.read().splitlines()
			self.user_id      = self.post_id[0].split('_')[0]
		except :
			print "error open file post-id"	
	
	
	
	#----------------------------------------------------------------------
	# Function sent email and password log in 
	def parse(self, response):
		return [FormRequest.from_response(response,formname='login_form',
				formdata={'email':self.email,'pass':self.password},callback=self.after_login)]
	
	
	
	
	#----------------------------------------------------------------------
	# Function after login and get Access Token
	def after_login(self, response):
		# log in error 
		if "pam login_error_box uiBoxRed" in response.body :
			self.log("Login failed", level=log.ERROR)
			return
		# log in complete
		else :
			'''
			# get access token 
			soup = BeautifulSoup(response.body)
			for load in soup.find_all('script'): 
				response_text = str(load.get_text())
				if re.search(r'graph_form',response_text) :
					split_text1 = re.split(r'graph_form',response_text)
					if re.search(r'require',split_text1[1]):
						split_text2 = re.split(r'\"require\"',split_text1[1])
						text = re.sub(r'[);]','',split_text2[1])
						graph_require = '{\"require\"' + text
						json_require = json.loads(graph_require)
						self.access_token = json_require["require"][1][3][2]
						break;
			'''
			for post in self.post_id:
				yield Request(url='https://graph.facebook.com/?id='+post+'&fields=likes&access_token='+self.access_token, callback=self.get_likes)
	
	
	
	#----------------------------------------------------------------------
	# Function get all comment of post-id 				
	def get_likes(self,response):
		
		# fist page 
		if (response.url).startswith('https://graph.facebook.com/?id=') :
			cut_filename = response.url.split('&fields=likes')[0]
			filename	 = re.sub(r'[https://graph.facebook.com/?id=]','',cut_filename)
		
		# padding like
		elif (response.url).startswith('https://graph.facebook.com/v2.0/') :
			cut_filename = re.sub(r'https://graph.facebook.com/v2.0/','',response.url)
			filename	 = cut_filename.split('/likes?')[0]
		
		check_path = 'Corpus/facebook/'+self.page_id+'/Check-Duplicated/'
		if not os.path.exists(check_path): 
			os.makedirs(check_path)	
		check_likes = check_path+'likes'+filename+'.txt'	
		append_likesOfPostId = open(check_likes,'a')
		likesOfPostId = open(check_likes).read().splitlines()
		
		if response.status == 200 :
			self.like = list()
			graph_data = json.loads(response.body)
			
			if len((graph_data.keys())) > 2 :  #first time
					
				if graph_data.has_key('likes'):
					for user_like in graph_data['likes']['data']:
						user_like['id'] = user_like['id'].decode('unicode-escape').encode('utf8')
						temp_data       = user_like['name']
						user_like['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
						
						if user_like['id'] not in check_likes : 	
							self.like.append(user_like)
							
					#print len(self.like)
					
					if filename in self.save_likes.keys():
						append_likesOfPostId.write(user_like['id']+'\n')
						self.save_likes[filename] += self.like
					else:
						self.save_likes[filename] = self.like
				if graph_data['likes'].has_key('paging'):
					if graph_data['likes']['paging'].has_key('next'):
						next_like = graph_data['likes']['paging']['next']
						yield Request(url=next_like, callback=self.get_likes)
					
			if len(graph_data.keys()) == 2 :
				
				if  graph_data.has_key('data') and graph_data['data'] != [] :
					for user_like in graph_data['data']:
						user_like['id'] = user_like['id'].decode('unicode-escape').encode('utf8')
						temp_data       = user_like['name']
						user_like['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
						if user_like['id'] not in check_likes : 
							self.like.append(user_like)
							append_likesOfPostId.write(user_like['id']+'\n')
					if filename in self.save_likes.keys():
						self.save_likes[filename] += self.like
					else:
						self.save_likes[filename] = self.like
						
					#print len(self.like)
				if graph_data.has_key('paging'):
					if graph_data['paging'].has_key('next'):
						next_like = graph_data['paging']['next']
						yield Request(url=next_like, callback=self.get_likes)
					
		path_file = 'Corpus/facebook/'+self.page_id+'/'+self.date+'/Like/'
		if not os.path.exists(path_file): 
			os.makedirs(path_file)	
		for postid in self.save_likes.keys():
			fname = path_file+postid
			save_json = open(fname,'wb')
			jss = json.dumps( self.save_likes[postid],indent=5).decode('unicode-escape').encode('utf8')
			for i in jss :	
				save_json.write(i)
