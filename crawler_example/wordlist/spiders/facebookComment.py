import json
import re
import os
import time
from scrapy.http import FormRequest
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
from scrapy import log
    
class facebookCommentSpider(Spider):
	
	#----------------------------------------------------------------------
	# Initial	
	def __init__(self,**kw):
		
		# get email, password, pagename
		self.email     = str(kw.get('email'))
		self.password  = str(kw.get('password'))  
		self.page_id = str(kw.get('pageId')) 
		self.access_token  = str(kw.get('access_token')) 

		# set start url, domain, declare global variable
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com/login.php']
		DOWNLOAD_DELAY  = 1.0
		CONCURRENT_REQUESTS = 10
		
		self.save_comments = dict()
		self.date = time.strftime("%d-%m-%y") # get current date
		self.path = '../InfoMine/Corpus/Corpus/Corpus/facebook/'+self.page_id+'/'+self.date
		try :	
			self.file_postid  = open(self.path+'/PostId'+self.date+'.txt')
			self.post_id      = self.file_postid.read().splitlines()
			self.user_id      = self.post_id[0].split('_')[0]
		except :
			print "error open file post-id"	
			
		
	
	#----------------------------------------------------------------------
	# Function sent email and password log in 
	def parse(self, response):
		
		# Form Reequest log in
		return [FormRequest.from_response(response,formname='login_form',
				formdata={'email':self.email,'pass':self.password},callback=self.after_login)]
	
	
	
	#----------------------------------------------------------------------
	# Function after login and get Access Token
	def after_login(self, response):
		# log in error 
		if "pam login_error_box uiBoxRed" in response.body :
			self.log("Login failed", level=log.ERROR)
			return
		# log in complete
		else :
			'''
			# get access token 
			soup = BeautifulSoup(response.body)
			for load in soup.find_all('script'): 
				response_text = str(load.get_text())
				if re.search(r'graph_form',response_text) :
					split_text1 = re.split(r'graph_form',response_text)
					if re.search(r'require',split_text1[1]):
						split_text2 = re.split(r'\"require\"',split_text1[1])
						text = re.sub(r'[);]','',split_text2[1])
						graph_require = '{\"require\"' + text
						json_require = json.loads(graph_require)
						self.access_token = json_require["require"][1][3][2]
						break;
			'''
			# crawl comment
			for post in self.post_id:
				yield Request(url='https://graph.facebook.com/?id='+post+'&fields=comments&access_token='+self.access_token, callback=self.get_comment)
	
	
	
	#----------------------------------------------------------------------
	# Function get all comment of post-id 			
	def get_comment(self,response):
		
		# check filename for fist page 
		if (response.url).startswith('https://graph.facebook.com/?id=') :
			cut_filename = response.url.split('&fields=comments')[0]
			filename	 = re.sub(r'[https://graph.facebook.com/?id=]','',cut_filename)
		
		# check filename for padding comment
		elif (response.url).startswith('https://graph.facebook.com/v2.0/') :
			cut_filename = re.sub(r'https://graph.facebook.com/v2.0/','',response.url)
			filename	 = cut_filename.split('/comments?')[0]
		
		# open path file check duplicate
		check_path = 'Corpus/facebook/'+self.page_id+'/Check-Duplicated/'
		if not os.path.exists(check_path): 
			os.makedirs(check_path)	
		check_comments = check_path+'comments'+filename+'.txt'	
		append_commentsOfPostId = open(check_comments,'a')
		commentsOfPostId = open(check_comments).read().splitlines()
		
		if response.status == 200 :
			self.comment = list()
			graph_data = json.loads(response.body)
			
			# first page 
			if len((graph_data.keys())) > 2 :  #first time
				if 'comments' in graph_data.keys():
					for user_comment in graph_data['comments']['data']:
						user_comment['id'] = user_comment['id'].decode('unicode-escape').encode('utf8')
						if user_comment['id'] not in check_comments : 
							append_commentsOfPostId.write(user_comment['id']+'\n')
							self.comment.append(user_comment)
					print len(self.comment)
					if filename in self.save_comments.keys():
						self.save_comments[filename] += self.comment
					else:
						self.save_comments[filename] = self.comment
				if 'next' in graph_data['comments']['paging'].keys():
					next_comment = graph_data['comments']['paging']['next']
					yield Request(url=next_comment, callback=self.get_comments)
		
			# padding 		
			if len(graph_data.keys()) == 2 :
				if graph_data['data'] != [] :
					for user_comment in graph_data['data']:
						user_comment['id'] = user_comment['id'].decode('unicode-escape').encode('utf8')
						if user_comment['id'] not in check_comments : 
							append_commentsOfPostId.write(user_comment['id']+'\n')
							self.comment.append(user_comment)
					if filename in self.save_comments.keys():
						self.save_comments[filename] += self.comment
					else:
						self.save_comments[filename] = self.comment
					print len(self.comment)
				if 'next' in graph_data['paging'].keys():
					next_comment = graph_data['paging']['next']
					yield Request(url=next_comment, callback=self.get_comments)
		
		# open path and save file comment			
		path_file = 'Corpus/facebook/'+self.page_id+'/'+self.date+'/Comments/'
		if not os.path.exists(path_file): 
			os.makedirs(path_file)	
		for k in self.save_comments.keys():
			print "save",k,len(self.save_comments[k])
			fname = path_file+k+'.txt'
			save_json = open(fname,'wb')
			jss = json.dumps( self.save_comments[k],indent=5).decode('unicode-escape').encode('utf8')
			for i in jss :	
				save_json.write(i)
