import json
import re
import os
import time
from scrapy.http import FormRequest
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
from scrapy import log

class facebookAdminPageSpider(Spider):

	#----------------------------------------------------------------------
	# Initial
	def __init__(self,**kw):
		print 'Hi'
		# set start url, domain, declare global variable
		name = "adminPage"
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com/login.php?next=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fexplorer%2F145634995501895%2F']
		self.access_token = ''
		self.admin_id = ''
		self.pageFacebook = dict()
		DOWNLOAD_DELAY = 0.50
		CONCURRENT_REQUESTS = 10
		
		# get email, password 
		self.email = kw.get('email')
		self.password = kw.get('password')   



	#----------------------------------------------------------------------
	# Function sent email and password log in 
	def parse(self, response):
		
		return [FormRequest.from_response(response,formname='login_form',
				formdata={'email':self.email,'pass':self.password},callback=self.after_login)]



	#----------------------------------------------------------------------
	# Function after login and get Access Token
	def after_login(self, response):
		
		# log in error 
		if "pam login_error_box uiBoxRed" in response.body :
			self.log("Login failed", level=log.ERROR)
			return
			
		# log in complete
		else :			
			# get access token from script
			'''
			soup = BeautifulSoup(response.body)
			for script in soup.find_all('script'): 
				response_text = str(script.get_text())
				if re.search(r'graph_form',response_text) :
					split_text1 = re.split(r'graph_form',response_text)
					if re.search(r'require',split_text1[1]):
						split_text2 = re.split(r'\"require\"',split_text1[1])
						text = re.sub(r'[);]','',split_text2[1])
						graph_require = '{\"require\"' + text
						json_require = json.loads(graph_require)
						self.access_token = json_require["require"][1][3][2]
						break;
			'''
			adminID_url = 'https://graph.facebook.com/v2.2/me?access_token='+self.access_token
			return Request(url = adminID_url , callback = self.get_AdminID)
	


	#----------------------------------------------------------------------
	# Function get Admin-ID
	def get_AdminID(self, response):
		admin_data = json.loads(response.body)
		self.admin_id = admin_data['id']
		pageID_url = 'https://graph.facebook.com/v2.2/'+self.admin_id+'/accounts?access_token='+self.access_token
		yield Request(url= pageID_url , callback=self.get_PageID)
	


	#----------------------------------------------------------------------
	# Function get Page's Admin
	def get_PageID(self, response): 
		
		page_data = json.loads(response.body)
		for page in page_data['data']:
			page_id = page['id']
			page_token = page['access_token']
			if page_id not in self.pageFacebook.keys() :
				self.pageFacebook[page_id] = page['name']
			postID_url = 'https://graph.facebook.com/v2.2/'+page_id+'/posts?fields=id&access_token='+page_token
			yield Request(url= postID_url, callback=self.get_PostID)
		


	#----------------------------------------------------------------------
	# Function get Post-ID of Page
	def get_PostID(self,response):
		
		# open path of post-id file
		cut_url = re.split('https://graph.facebook.com/v|https://graph.facebook.com',response.url)[1]
		page_id = cut_url.split('/')[1]
		page_name = self.pageFacebook[page_id]
		page_name = re.sub(' ','_',page_name)
		date = time.strftime("%d-%m-%y") # get current date
		save_path = '../InfoMine/Corpus/facebook/'+page_name+'/'+date
		if not os.path.exists(save_path): 
			os.makedirs(save_path)	
		filename = os.path.join(save_path,'PostId'+date+'.txt')	
		write_PostId = open(filename,'a')
		
		# get post-id's page-id and write post-id
		data = json.loads(response.body)
		if data['data'] != []:
			for i in data['data']:
				post_id = i['id']+"\n"
				write_PostId.write(post_id)
				#print post_id
			try :
				nextpage = data['paging']['next']
				yield Request(url=nextpage, callback=self.get_PostID)
			except :
				nextpage = "last page"
