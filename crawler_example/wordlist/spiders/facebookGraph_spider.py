 #!/usr/bin/python
 # -*- coding: UTF-8 -*-
 
import json
import re
import os
import time
from scrapy.http import FormRequest
from scrapy.spider import Spider
from bs4 import BeautifulSoup,Comment
from scrapy.http import Request
from scrapy import log
    
class facebookGraphSpider(Spider):
	
	#----------------------------------------------------------------------
	# Initial	
	def __init__(self,**kw):
		
		# get email, password, pagename
		self.email = str(kw.get('email'))
		self.password = str(kw.get('password'))  
		self.page_id = str(kw.get('pageName')) 
		self.access_token = str(kw.get('access_token'))
		 
		# set start url, domain, declare global variable
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com/login.php']
		DOWNLOAD_DELAY = 1.0
		CONCURRENT_REQUESTS = 10
		 
		
		self.date = time.strftime("%d-%m-%y")
		self.path = '../InfoMine/Corpus/facebook/'+self.page_id+'/'+self.date+'/'
		try	:
			self.file_postid  = open(self.path+'PostId'+self.date+'.txt')
			self.post_id      = self.file_postid.read().splitlines()
			self.user_id      = self.post_id[0].split('_')[0]
		
		except :
			print "error open file post-id"	
			
			
		
	#----------------------------------------------------------------------
	# Function sent email and password log in 
	def parse(self, response):
		
		# Form Reequest log in
		return [FormRequest.from_response(response,formname='login_form',
				formdata={'email':self.email,'pass':self.password},callback=self.after_login)]
	
	
	
	#----------------------------------------------------------------------
	# Function after login and get Access Token
	def after_login(self, response):
		# log in error 
		if "pam login_error_box uiBoxRed" in response.body :
			self.log("Login failed", level=log.ERROR)
			return
		# log in complete
		else :
			'''
			# get access token 
			soup = BeautifulSoup(response.body)
			for load in soup.find_all('script'): 
				response_text = str(load.get_text())
				if re.search(r'graph_form',response_text) :
					split_text1 = re.split(r'graph_form',response_text)
					if re.search(r'require',split_text1[1]):
						split_text2 = re.split(r'\"require\"',split_text1[1])
						text = re.sub(r'[);]','',split_text2[1])
						graph_require = '{\"require\"' + text
						json_require = json.loads(graph_require)
						self.access_token = json_require["require"][1][3][2]
						break;
			'''
			for post in self.post_id:
				yield Request(url='https://graph.facebook.com/?id='+post+'&access_token='+self.access_token, callback=self.get_post)	
	
	#----------------------------------------------------------------------
	# Function get graph API				
	def get_post(self,response):
		# path save file json
		name = re.sub(r'[https://graph.facebook.com/?id=]','',response.url.split('&access_token=')[0])
		
		folder = name.split('_')[0]
		save_file = self.path+'/Post/'
		if not os.path.exists(save_file): 
			print  "crawl page -> "+ self.page_id
			os.makedirs(save_file)
		complete_name = os.path.join(save_file,name)
		save_json = open(complete_name,'w')
		
		graph_data = json.loads(response.body)
			
		if graph_data.has_key('message'):
			temp_data             = graph_data['message']
			graph_data['message'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
			
		if graph_data.has_key('name'):
			temp_data             = graph_data['name']
			graph_data['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
	
		if graph_data.has_key('description'):
			temp_data             = graph_data['description']
			graph_data['description'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
			
		if graph_data.has_key('story'):
			temp_data             = graph_data['story']
			graph_data['story'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
		
		if graph_data.has_key(' caption'):
			temp_data             = graph_data[' caption']
			graph_data[' caption'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
							
		if graph_data.has_key('comments'):
			comment_count = len(graph_data['comments']['data'])
			for index in range(0,comment_count) :
				temp_data       = graph_data['comments']['data'][index]['message']
				graph_data['comments']['data'][index]['message'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
				
				temp_data       = graph_data['comments']['data'][index]['from']['name']
				graph_data['comments']['data'][index]['from']['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
		
		if graph_data.has_key('likes'):
			like_count = len(graph_data['likes']['data'])
			for index in range(0,like_count) :
				temp_data       = graph_data['likes']['data'][index]['name']
				graph_data['likes']['data'][index]['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
		
		if graph_data.has_key('to'):
			like_count = len(graph_data['to']['data'])
			for index in range(0,like_count) :
				temp_data       = graph_data['to']['data'][index]['name']
				graph_data['to']['data'][index]['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
				
		if graph_data.has_key('story_tags'):
			for index in graph_data['story_tags'] :
				list_tag_key = len(graph_data['story_tags'][index])
				for i in range(0,list_tag_key) :
					temp_data       = graph_data['story_tags'][index][i]['name']
					graph_data['story_tags'][index][i]['name'] = re.sub(r'[ \s\r\n\t\{\}\(\)\,\"\'\\\:\[\]]',' ',temp_data)
	
		try:
			
			jss = json.dumps(graph_data,indent=5,sort_keys=True).decode('unicode-escape').encode('utf8')			
			# write json data
			for i in jss :	
				save_json.write(i)
		except :
			error = open(self.path+'/error','a')
			error.write(name)
		
			
