import json
import re
import os
import time
from scrapy.http import FormRequest
from scrapy.spider import Spider
from bs4 import BeautifulSoup
from scrapy.http import Request
from scrapy import log

class facebookBotSpider(Spider):
	
	#----------------------------------------------------------------------
	# Initial
	def __init__(self,**kw):
		
		# set start url, domain, declare global variable
		name = "bot"
		self.allowed_domains = ["facebook.com"]
		self.start_urls = ['https://www.facebook.com/login.php']
		self.access_token = ''
		self.admin_id = ''
		self.page_name = ''
		DOWNLOAD_DELAY = 0.5
		CONCURRENT_REQUESTS = 10
		
		# get email, password, page-id 
		self.email = kw.get('email')
		self.password = kw.get('password') 
		self.page_id = kw.get('page_id') 
		self.access_token = kw.get('access_token')
		
		
	#----------------------------------------------------------------------
	# Function sent email and password log in 			
	def parse(self, response):
		
		# Form Reequest log in
		return [FormRequest.from_response(response,formname='login_form',
				formdata={'email':self.email,'pass':self.password},callback=self.after_login)]
	
	
	
	#----------------------------------------------------------------------
	# Function after login and get Access Token
	def after_login(self, response):
		
		# log in error 
		if "pam login_error_box uiBoxRed" in response.body :
			self.log("Login failed", level=log.ERROR)
			return
		# log in complete
		else :
			print "login "
			'''
			# get access token 
			soup = BeautifulSoup(response.body)
			for load in soup.find_all('script'): 
				response_text = str(load.get_text())
				if re.search(r'graph_form',response_text) :
					split_text1 = re.split(r'graph_form',response_text)
					if re.search(r'require',split_text1[1]):
						split_text2 = re.split(r'\"require\"',split_text1[1])
						text = re.sub(r'[);]','',split_text2[1])
						graph_require = '{\"require\"' + text
						json_require = json.loads(graph_require)
						self.access_token = json_require["require"][1][3][2]
						break;
			'''
			page_url = 'https://graph.facebook.com/'+self.page_id+'/posts?fields=id&access_token='+self.access_token
			return Request(url = page_url, callback = self.get_PostID)
	
	
	'''
	#----------------------------------------------------------------------
	# Function get name of page			
	def get_PageName(self, response):
		
		# Page data
		page_data = json.loads(response.body)
		self.page_name = page_data['name']
		self.page_name = re.sub(' ','_',self.page_name)
		postID_url = 'https://graph.facebook.com/'+self.page_id+'/posts?fields=id&access_token='+self.access_token
		yield Request(url= postID_url , callback=self.get_PostID)
	'''
	
	#----------------------------------------------------------------------
	# Function get Post-ID of Page 	
	def get_PostID(self,response):			
		
		# open path of post-id file	
		date = time.strftime("%d-%m-%y") # get current date
		save = 'Corpus/facebook/'+self.page_id+'/'+date+'/'
		if not os.path.exists(save): 
			os.makedirs(save)	
		filename = os.path.join(save,'PostId'+date+'.txt')
		write_PostId = open(filename,'a')
		
		# get post-id's admin and write post-id	
		data = json.loads(response.body)
		if data['data'] != []:
			for i in data['data']:
				post_id = i['id']+"\n"
				write_PostId.write(post_id)
				print post_id
			try :
				nextpage = data['paging']['next']
				yield Request(url=nextpage, callback=self.get_PostID)
			except :
				nextpage = "last page"
			
