# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WordlistItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    topic = scrapy.Field()
    url_topic = scrapy.Field()
    name = scrapy.Field()
    date = scrapy.Field()
    time = scrapy.Field()
    tag = scrapy.Field()
    pass
