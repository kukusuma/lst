import sys
from twisted.internet import reactor
from scrapy.crawler import Crawler
from scrapy import log, signals
from wordlist.spiders.facebookAdminPage_spider import facebookAdminPageSpider
from scrapy.utils.project import get_project_settings

def crawl_postid(u,p):
	spider = facebookAdminPageSpider(email=u,password=p) #set spider 
	settings = get_project_settings()
	crawler = Crawler(settings)
	crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
	crawler.configure()
	crawler.crawl(spider) #crawl spider
	crawler.start()
	log.start()
	reactor.run() 
	
email = sys.argv[1]
password = sys.argv[2]
print " crawl post-id's Page ID ... "
crawl_postid(email,password)
