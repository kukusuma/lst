import sys
from twisted.internet import reactor, task
from scrapy.crawler import Crawler 
from scrapy import log, signals
from wordlist.spiders.facebookComment import facebookCommentSpider
from scrapy.utils.project import get_project_settings

def crawl_postid(u,p,n):
	spider = facebookCommentSpider(email=u,password=p,pageName=n) #set spider 
	settings = get_project_settings()
	crawler = Crawler(settings)
	crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
	crawler.configure()
	crawler.crawl(spider) #crawl spider
	crawler.start()
	#log.start()
	reactor.run() 
	
email = sys.argv[1]
password = sys.argv[2]
page_name = sys.argv[3]
crawl_postid(email,password,page_name)
